#!/bin/sh

echo -n "Enter Ruby URL : "
read ruby_url
cd $HOME
echo "各種ツールのインストールを開始します"
yum install -y xinetd emacs zsh openssl openssl-devel make gcc zlib-devel gettext git nginx readline gcc-c++ curl curl-devel mysql mysqk-devel vsftpd libxml2 libxm2-devel libxslt libxslt-devel wget
cp ./zshrc $HOME
cp ./zshrc /etc/skel
cp -r ./.emacs.d $HOME
cp -r ./.emacs.d /etc/skel
echo "Ruby のダウンロードを開始します"
mkdir ruby
cd ruby
wget $ruby_url

if [[ "$ruby_url" =~ (ruby-[0-9].[0-9].[0-9]-p[0-9]+)(.tar.gz)$ ]] ; then
  filename=${BASH_REMATCH[1]}
  ext_name=${BASH_REMATCH[2]}
  echo "解凍を開始します"
  tar -xzvf $filename$ext_name
  echo "ディレクトリ $filename に入ります"
  cd $filename
  ./configure
  make
  make install
  cd ./ext/openssl
  ruby ./extconf.rb
  make install
  gem install rails --no-ri --no-rdoc
fi

echo "セットアップが完了しました"
